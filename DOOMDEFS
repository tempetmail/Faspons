/*
--------------------------------------------------
GENERIC
--------------------------------------------------
*/

//Generic Explosion
pulselight EXPLOSION_FLASH
{
	Color 1.0 0.3 0.0
	Size 100
	SecondarySize 120
	Interval 0.1
	Offset 0 60 0
}

/*
--------------------------------------------------
WEAPONS
--------------------------------------------------
*/

// Bullet puffs
flickerlight BPUFF1
{
    color 0.5 0.5 0.0
    size 6
    secondarySize 8
    chance 0.8
}

flickerlight BPUFF2
{
    color 0.5 0.5 0.0
    size 3
    secondarySize 4
    chance 0.8
}

object ShotgunPuff
{
    frame PUFFA { LIGHT BPUFF1 }
    frame PUFFB { LIGHT BPUFF2 }
}

object SShotgunPuff
{
    frame PUFFA { LIGHT BPUFF1 }
    frame PUFFB { LIGHT BPUFF2 }
}


object NatoPuff
{
    frame PUFFA { LIGHT BPUFF1 }
    frame PUFFB { LIGHT BPUFF2 }
}

object PuffParticles
{
	frame PUFFA { LIGHT BPUFF1 }
	frame PUFFB { LIGHT BPUFF2 }
}

object PuffSmoke
{
	frame PUFFA { LIGHT BPUFF1 }
	frame PUFFB { LIGHT BPUFF2 }
}

//Muzzle flashes
pointlight MUZZLEFLASH
{
    color 0.5 0.4 0
    size 48
}

object MuzzFl
{
	frame TNT1 { light MUZZLEFLASH }
}

pointlight BFGMUZZLEFLASH
{
    color 0.0 1.0 0.0
    size 48
}

object BFGMuzzFl
{
	frame TNT1 { light BFGMUZZLEFLASH }
}

// Rocket
pulselight ROCKETLIGHT
{
    color 1.0 0.3 0.0
    size 48
    secondarySize 54
    interval 0.1
    offset 0 0 0
}

flickerlight ROCKET_X1
{
    color 1.0 0.7 0.0
    size 64
    secondarySize 72
    chance 0.3
}

flickerlight ROCKET_X2
{
    color 0.5 0.1 0.0
    size 80
    secondarySize 88
    chance 0.3
}

flickerlight ROCKET_X3
{
    color 0.3 0.0 0.0
    size 96
    secondarySize 104
    chance 0.3
}

object RPGRocket
{
	frame MSLE { LIGHT ROCKETLIGHT }

	frame MACXA { LIGHT ROCKET_X1 }
	frame MACXB { LIGHT ROCKET_X1 }
	frame MACXC { LIGHT ROCKET_X2 }
	frame MACXD { LIGHT ROCKET_X3 }
	frame MACXE { LIGHT ROCKET_X3 }
	frame MACXF { LIGHT ROCKET_X2 }
	frame MACXG { LIGHT ROCKET_X1 }
	frame MACXH { LIGHT ROCKET_X1 }
	frame MACXI { LIGHT ROCKET_X1 }
	frame MACXJ { LIGHT ROCKET_X1 }
}

object CyberRocket
{
	frame MSLE { LIGHT ROCKETLIGHT }

	frame MACXA { LIGHT ROCKET_X1 }
	frame MACXB { LIGHT ROCKET_X1 }
	frame MACXC { LIGHT ROCKET_X2 }
	frame MACXD { LIGHT ROCKET_X3 }
	frame MACXE { LIGHT ROCKET_X3 }
	frame MACXF { LIGHT ROCKET_X2 }
	frame MACXG { LIGHT ROCKET_X1 }
	frame MACXH { LIGHT ROCKET_X1 }
	frame MACXI { LIGHT ROCKET_X1 }
	frame MACXJ { LIGHT ROCKET_X1 }
}

//Plasma

pointlight PLASMABALL
{
    color 0.0 0.1 1.0
    size 128
}

object Plasma
{
    frame PLZSA { light PLASMABALL }
	frame PLZSB { light PLASMABALL }
}

//BFG

pointlight BFGBALL
{
    color 0.0 1.0 0.0
    size 128
}

Object HyperNova
{
	frame BBALA { light BFGBALL }
	frame BBALB { light BFGBALL }

	frame BBB1A { light BFGBALL_X1 }
	frame BBB1B { light BFGBALL_X2 }
	frame BBB1C { light BFGBALL_X3 }
	frame BBB1D { light BFGBALL_X4 }
	frame BBB1E { light BFGBALL_X5 }

	frame RR00 { light BFGBALL }
}

Object HyperNova2
{
	frame BBALA { light BFGBALL }
	frame BBALB { light BFGBALL }

	frame BBB1A { light BFGBALL_X1 }
	frame BBB1B { light BFGBALL_X2 }
	frame BBB1C { light BFGBALL_X3 }
	frame BBB1D { light BFGBALL_X4 }
	frame BBB1E { light BFGBALL_X5 }

	frame RR00 { light BFGBALL }
}

Object HyperNova3
{
	frame BBALA { light BFGBALL }
	frame BBALB { light BFGBALL }

	frame BBB1A { light BFGBALL_X1 }
	frame BBB1B { light BFGBALL_X2 }
	frame BBB1C { light BFGBALL_X3 }
	frame BBB1D { light BFGBALL_X4 }
	frame BBB1E { light BFGBALL_X5 }

	frame RR00 { light BFGBALL }
}

Object HyperNova4
{
	frame BBALA { light BFGBALL }
	frame BBALB { light BFGBALL }

	frame BBB1A { light BFGBALL_X1 }
	frame BBB1B { light BFGBALL_X2 }
	frame BBB1C { light BFGBALL_X3 }
	frame BBB1D { light BFGBALL_X4 }
	frame BBB1E { light BFGBALL_X5 }

	frame RR00 { light BFGBALL }
}

/*
--------------------------------------------------
MONSTERS
--------------------------------------------------
*/

pointlight IMPFIREBALL
{
    color 1.0 0.5 0.0
    size 64
}

// Doom imp fireball explosion
flickerlight IMPFIREBALL_X1
{
    color 0.7 0.2 0.0
    size 80
    secondarySize 88
    chance 0.25
}

flickerlight IMPFIREBALL_X2
{
    color 0.4 0.0 0.0
    size 96
    secondarySize 104
    chance 0.25
}

flickerlight IMPFIREBALL_X3
{
    color 0.2 0.0 0.0
    size 112
    secondarySize 120
    chance 0.25
}

object ImpFireball
{
	frame BAL1A { LIGHT IMPFIREBALL }
	frame BAL1B { LIGHT IMPFIREBALL }
	frame BAL1C { LIGHT IMPFIREBALL_X1 }
	frame BAL1D { LIGHT IMPFIREBALL_X2 }
	frame BAL1E { LIGHT IMPFIREBALL_X3 }
}

//Cacodemon fireball
flickerlight CACOBALL
{
    color 0.1 0.1 1.0
    size 56
    secondarySize 64
    chance 0.5
}

flickerlight CACOBALL_X1
{
    color 0.1 0.4 0.9
    size 72
    secondarySize 80
    chance 0.25
}

flickerlight CACOBALL_X2
{
    color 0.0 0.1 0.6
    size 88
    secondarySize 96
    chance 0.25
}

flickerlight CACOBALL_X3
{
    color 0.0 0.0 0.3
    size 104
    secondarySize 112
    chance 0.25
}

object CacodemonBallRemix
{
    frame BAL2A { light CACOBALL }
    frame BAL2B { light CACOBALL }

    frame BAL2C { light CACOBALL_X1 }
    frame BAL2D { light CACOBALL_X2 }
	frame BAL2E { light CACOBALL_X2 }
    frame BAL2F { light CACOBALL_X3 }
}

//Lost soul
flickerlight LOSTSOUL
{
    color 1.0 0.3 0.0
    size 56
    secondarysize 64
    chance 0.5
}


flickerlight LOSTSOUL_X1
{
    color 0.8 0.3 0.0
    size 72
    secondarySize 80
    chance 0.25
}

flickerlight LOSTSOUL_X2
{
    color 0.6 0.2 0.0
    size 88
    secondarySize 96
    chance 0.25
}

flickerlight LOSTSOUL_X3
{
    color 0.4 0.1 0.0
    size 104
    secondarySize 112
    chance 0.25
}

flickerlight LOSTSOUL_X4
{
    color 0.2 0.0 0.0
    size 112
    secondarySize 120
    chance 0.25
}

object LostSoulRemix
{
    frame SKULA { light LOSTSOUL_X1 }
    frame SKULB { light LOSTSOUL_X1 }
    frame SKULC { light LOSTSOUL_X1 }
    frame SKULD { light LOSTSOUL_X1 }
    frame SKULE { light LOSTSOUL_X1 }
    frame SKULF { light LOSTSOUL_X1 }
    frame SKULG { light LOSTSOUL_X1 }
    frame SKULH { light LOSTSOUL_X1 }
    frame SKULI { light LOSTSOUL_X2 }
    frame SKULJ { light LOSTSOUL_X3 }
    frame SKULK { light LOSTSOUL_X4 }
}

// Mancubus Fireball
object FatShot
{
    frame MANFA { light ROCKETLIGHT }
    frame MANFB { light IMPBALL }

    frame MISLB { light ROCKET_X1 }
    frame MISLC { light ROCKET_X2 }
    frame MISLD { light ROCKET_X3 }
}

object FatShotFast
{
    frame MANFA { light ROCKETLIGHT }
    frame MANFB { light IMPBALL }

    frame MISLB { light ROCKET_X1 }
    frame MISLC { light ROCKET_X2 }
    frame MISLD { light ROCKET_X3 }
}

// Revenant tracer
pointlight TRACER
{
    color 1.0 0.3 0.0
    size 48
}

flickerlight TRACER_X1
{
    color 1.0 0.2 0.0
    size 64
    secondarySize 72
    chance 0.25
}

flickerlight TRACER_X2
{
    color 0.6 0.0 0.0
    size 80
    secondarySize 88
    chance 0.25
}

flickerlight TRACER_X3
{
    color 0.3 0.0 0.0
    size 96
    secondarySize 104
    chance 0.25
}

object HomingRevenantMissile {
	frame FATB { light TRACER }

	frame FBXPA { light TRACER_X1 }
	frame FBXPB { light TRACER_X2 }
	frame FBXPC { light TRACER_X3 }
}

object FastRevenantMissile {
	frame FATB { light TRACER }

	frame FBXPA { light TRACER_X1 }
	frame FBXPB { light TRACER_X2 }
	frame FBXPC { light TRACER_X3 }

}

object RevenantRemix
{
	frame SKELJ { light TRACER_X1 }
	frame SKELK { light TRACER_X1 }
}

//Baron / Hellknight
pointlight BARONBALL
{
    color 0.0 1.0 0.0
    size 64
}

flickerlight BARONBALL_X1
{
    color 0.0 0.7 0.0
    size 80
    secondarySize 88
    chance 0.25
}

flickerlight BARONBALL_X2
{
    color 0.0 0.4 0.0
    size 96
    secondarySize 104
    chance 0.25
}

flickerlight BARONBALL_X3
{
    color 0.0 0.2 0.0
    size 112
    secondarySize 120
    chance 0.25
}

object BaronBall {
	frame BAL7A { light BARONBALL }
	frame BAL7B { light BARONBALL }

	frame BAL7C { light BARONBALL_X1 }
	frame BAL7D { light BARONBALL_X2 }
	frame BAL7E { light BARONBALL_X3 }
}

//Arachnotron
pointlight ARACHPLAS
{
    color 0.6 1.0 0.0
    size 56
}

flickerlight ARACHPLAS_X1
{
    color 0.4 0.8 0.0
    size 72
    secondarySize 80
    chance 0.3
}

flickerlight ARACHPLAS_X2
{
    color 0.6 0.6 0.0
    size 88
    secondarySize 96
    chance 0.3
}

flickerlight ARACHPLAS_X3
{
    color 0.4 0.4 0.0
    size 48
    secondarySize 32
    chance 0.3
}

flickerlight ARACHPLAS_X4
{
    color 0.2 0.2 0.0
    size 24
    secondarySize 16
    chance 0.3
}

object ArachnotronPlasma {
	frame APLS { light ARACHPLAS }

	frame APBXA { light ARACHPLAS_X1 }
	frame APBXB { light ARACHPLAS_X1 }
	frame APBXC { light ARACHPLAS_X2 }
	frame APBXD { light ARACHPLAS_X3 }
	frame APBXE { light ARACHPLAS_X4 }
}

//Archvile Flame
object VileFlame {
	frame FIRE { light IMPFIREBALL }
}

/*
--------------------------------------------------
ITEMS
--------------------------------------------------
*/

pulselight GREENGOOZ
{
    color 0.3 1.0 0.0
    size 32
    secondarySize 34
    interval 0.1
    offset 0 40 0
	dontlightself 1
}

object ToxicBarrel {
	frame BAR1 { light GREENGOOZ }
	frame BARS { light GREENGOOZ }

	frame BEXPA { LIGHT ROCKET_X1 }
	frame BEXPB { LIGHT ROCKET_X1 }
	frame BEXPC { LIGHT ROCKET_X2 }
	frame BEXPD { LIGHT ROCKET_X2 }
	frame BEXPE { LIGHT ROCKET_X3 }
}

pulselight GREENITEM
{
    color 0.3 1.0 0.0
    size 14
    secondarySize 16
    interval 0.1
    offset 0 5 0
}

pulselight BLUEITEM
{
    color 0.1 0.1 1.0
    size 14
    secondarySize 16
    interval 0.1
    offset 0 5 0
}

// Soul Sphere
pulselight SOULSPHERE
{
    color 0.0 0.0 1.0
    size 40
    secondarySize 42
    interval 2.0
    offset 0 16 0
}

object Spiritsphere
{
    frame SOUL { light SOULSPHERE }
}

// Invulnerability Sphere
pulselight INVULN
{
    color 0.0 1.0 0.0
    size 40
    secondarySize 42
    interval 2.0
    offset 0 16 0
}

object GodSphere {
	frame PINV { light INVULN }
}

pulselight HEALTHPOTION
{
    color 0.0 0.0 0.6
    size 16
    secondarySize 18
    interval 2.0
}

object HealthBonus
{
    frame BON1 { light BLUEITEM }
}

// Armour Helmet
pulselight ARMORBONUS
{
    color 0.0 0.6 0.0
    size 14
    secondarySize 16
    interval 1.0
}

object ArmourBonus {
	frame BON2 { light ARMORBONUS }
}

// Yellow Keys
pulselight YELLOWKEY
{
    color 0.6 0.6 0.0
    size 16
    secondarySize 18
    interval 2.0
}

object YellowCardRemix {
	frame YKEY { light YELLOWKEY }
}

object YellowSkullRemix {
	frame YSKU { light YELLOWKEY }
}

// Red Keys
pulselight REDKEY
{
    color 0.6 0.0 0.0
    size 16
    secondarySize 18
    interval 2.0
}

object RedCardRemix {
	frame RKEY { light REDKEY }
}

object RedSkullRemix {
	frame RSKU { light REDKEY }
}

//Blue Keys
pulselight BLUEKEY
{
    color 0.0 0.0 0.6
    size 16
    secondarySize 18
    interval 2.0
}

object BlueCardRemix {
	frame BKEY { light BLUEKEY }
}

object BlueSkullRemix {
	frame BSKU { light BLUEKEY }
}

// Green armour
pointlight GREENARMOR1
{
    color 0.0 0.6 0.0
    size 48
}

pointlight GREENARMOR2
{
    color 0.0 0.6 0.0
    size 32
}

object GreenArmour
{
    frame ARM1A { light GREENARMOR1 }
	frame ARM1B { light GREENARMOR2 }
}

// Blue armour
pointlight BLUEARMOR1
{
    color 0.0 0.0 0.6
    size 48
}

pointlight BLUEARMOR2
{
    color 0.0 0.0 0.6
    size 32
}

object BlueArmour
{
    frame ARM2A { light BLUEARMOR1 }
	frame ARM2B { light BLUEARMOR2 }
}

/*
--------------------------------------------------
DECORATIONS
--------------------------------------------------
*/

// Floor lamp
pointlight LAMP
{
    color 1.0 1.0 1.0
    size 56
    offset 0 44 0
}

pointlight YELLOWLAMP
{
    color 1.0 1.0 0.0
    size 56
    offset 0 44 0
}

object Column
{
    frame COLUA { light YELLOWLAMP }
}


// Short tech lamp
pulselight SMALLLAMP
{
    color 0.8 0.8 1.0
    size 56
    secondarySize 58
    interval 0.4
    offset 0 44 0
}

object TechLamp2
{
    frame TLP2A { light SMALLLAMP }
    frame TLP2B { light SMALLLAMP }
    frame TLP2C { light SMALLLAMP }
    frame TLP2D { light SMALLLAMP }
}

// Tall tech lamp
pulselight BIGLAMP
{
    color 0.8 0.8 1.0
    size 64
    secondarySize 66
    interval 0.4
    offset 0 72 0
}

object TechLamp
{
    frame TLMPA { light BIGLAMP }
    frame TLMPB { light BIGLAMP }
    frame TLMPC { light BIGLAMP }
    frame TLMPD { light BIGLAMP }
}

// Tall red torch
pulselight BIGREDTORCH
{
    color 1.0 0.3 0.0
    size 58
    secondarySize 64
    interval 0.1
    offset 0 60 0
}

object RedTorch
{
    frame TTRB { light BIGREDTORCH }
}

// Tall green torch
pulselight BIGGREENTORCH
{
    color 0.0 1.0 0.0
    size 58
    secondarySize 64
    interval 0.1
    offset 0 60 0
}

object GreenTorch
{
    frame TTRB { light BIGGREENTORCH }
}

// Tall blue torch
pulselight BIGBLUETORCH
{
    color 0.0 0.0 1.0
    size 58
    secondarySize 64
    interval 0.1
    offset 0 60 0
}

object BlueTorch
{
    frame TTRB { light BIGBLUETORCH }
}

// Small red torch
pulselight SMALLREDTORCH
{
    color 1.0 0.3 0.0
    size 48
    secondarySize 54
    interval 0.1
    offset 0 35 0
}

object ShortRedTorch
{
    frame STRB { light SMALLREDTORCH }
}

// Small green torch
pulselight SMALLGREENTORCH
{
    color 0.0 1.0 0.0
    size 48
    secondarySize 54
    interval 0.1
    offset 0 35 0
}

object ShortGreenTorch
{
    frame STRB { light SMALLGREENTORCH }
}

// Small blue torch
pulselight SMALLBLUETORCH
{
    color 0.0 0.0 1.0
    size 48
    secondarySize 54
    interval 0.1
    offset 0 35 0
}

object ShortBlueTorch
{
    frame STRB { light SMALLBLUETORCH }
}

object HeadCandles
{
    frame POL3 { light SKULLCANDLES }
}

Glow
{
	Flats
	{
		BLOOD1
		BLOOD2
		BLOOD3

		FLAT17
		FLAT2
		FLAT22
		FLOOR1_7
		RROCK01
		RROCK02

		RROCK05
		RROCK06
		RROCK07
		RROCK08

		tlite6_1
		tlite6_4
		grnlite1
		ceil3_4
		ceil3_3
		floor1_7
		gate4
		gate1
		gate3
		TLITE6_6

		NUKAGE1
		NUKAGE2
		NUKAGE3
		LAVA1
		LAVA2
		LAVA3
		LAVA4
		DOORBLU
		DOORBLU2
		DOORRED
		DOORRED2
		DOORYEL
		DOORYEL2

		SLIME04
		SLIME03
		SLIME02
		SLIME01
	}
}
