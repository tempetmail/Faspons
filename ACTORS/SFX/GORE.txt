ACTOR RedNewBloodHitFlying {
	Game Doom
	scale 0.5
	speed 3
	health 1
	radius 8
	height 1
	Gravity 0.3
	damage 0
	Renderstyle Translucent
	Alpha 0.95
	DamageType Blood
	Decal BrutalBloodSplat
	+MISSILE
	+CLIENTSIDEONLY
	+NOCLIP
	+NOTELEPORT
	+NOBLOCKMAP
	+BLOODLESSIMPACT
	+FORCEXYBILLBOARD
	+NODAMAGETHRUST
	+MOVEWITHSECTOR
	+CORPSE
	-DONTSPLASH
	+THRUACTORS
	-SOLID
	Mass 1
	States {
		Spawn:
			BLOD KKKKKKKKKKKK 1 A_FadeOut(0.05)
			Stop
		Death:
			TNT1 A 0
			Stop
	}
}

ACTOR BlueNewBloodHitFlying {
	Game Doom
	scale 0.5
	speed 3
	health 1
	radius 8
	height 1
	Gravity 0.9
	damage 0
	Renderstyle Translucent
	Alpha 0.95
	DamageType Blood
	Decal BrutalBloodSplat
	Translation "1:191=200:207","208:255=200:207"
	+MISSILE
	+CLIENTSIDEONLY
	+NOCLIP
	+NOTELEPORT
	+NOBLOCKMAP
	+BLOODLESSIMPACT
	+FORCEXYBILLBOARD
	+NODAMAGETHRUST
	+MOVEWITHSECTOR
	+CORPSE
	-DONTSPLASH
	+THRUACTORS
	-SOLID
	Mass 1
	States {
		Spawn:
			BLOD KKKKKKKKKKKK 1 A_FadeOut(0.05)
			Stop
		Death:
			TNT1 A 0
			Stop
	}
}

ACTOR GreenNewBloodHitFlying {
	Game Doom
	scale 0.5
	speed 3
	health 1
	radius 8
	height 1
	Gravity 0.3
	damage 0
	Renderstyle Translucent
	Alpha 0.95
	DamageType Blood
	Decal BrutalBloodSplat
	Translation "168:184=112:127","185:191=11:12","16:47=112:127"
	+MISSILE
	+CLIENTSIDEONLY
	+NOCLIP
	+NOTELEPORT
	+NOBLOCKMAP
	+BLOODLESSIMPACT
	+FORCEXYBILLBOARD
	+NODAMAGETHRUST
	+MOVEWITHSECTOR
	+CORPSE
	-DONTSPLASH
	+THRUACTORS
	-SOLID
	Mass 1
	States {
		Spawn:
			BLOD KKKKKKKKKKKK 1 A_FadeOut(0.05)
			Stop
		Death:
			TNT1 A 0
			Stop
	}
}

Actor RedBlood {
	+FORCEXYBILLBOARD
	+GHOST
	+NOBLOCKMAP
	+NOGRAVITY
	+NOCLIP
	-DONTSPLASH
	+ALLOWPARTICLES
	-SOLID
	Decal BloodSplat
	Speed 0
	States {
		Spawn:
			TNT1 AA 0 A_SpawnProjectile ("RedFlyingBlood", 0, 0, random (0, 360), 2, random (0, 160))
			TNT1 AA 0 A_SpawnProjectile ("RedFlyingBloodFast", 0, 0, random (0, 360), 2, random (50, 120))
			BLUD CBA 2
			Stop
	}
}

ACTOR RedFlyingBlood {
	-DOOMBOUNCE
	-SOLID
	+THRUACTORS
	-DONTSPLASH
	+NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	+DONTOVERLAP
	decal bloodsplat
	Speed 4
	Scale 1.0
	Gravity 0.7
	Mass 1
	Radius 3
  Height 3
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "spawn1", "spawn2")
		Spawn1:
			BLUD F 5
			Loop
		Spawn2:
			BLUD G 5
			Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3", "Death4")
		 Death1:
		 	GIBS B 1
			TNT1 A 0 A_QueueCorpse
			GIBS B -1
			Stop
		Death2:
			BLD1 D 1
			TNT1 A 0 A_QueueCorpse
			BLD1 D -1
			Stop
		Death3:
			BLD1 C 1
			TNT1 A 0 A_QueueCorpse
			BLD1 C -1
			Stop
		Death4:
			BLUD F 1
			TNT1 A 0 A_QueueCorpse
			BLUD F -1
			Stop
		}
}

Actor BlueBlood : RedBlood {
	Decal BlueBloodSplat
	Translation "1:191=200:207","208:255=200:207"
	States {
		Spawn:
			TNT1 AA 0 A_SpawnProjectile ("BlueFlyingBlood", 0, 0, random (0, 360), 2, random (0, 160))
			TNT1 AA 0 A_SpawnProjectile ("BlueFlyingBloodFast", 0, 0, random (0, 360), 2, random (0, 180))
			BLUD ABC 2
			Stop
		}
}

Actor GreenBlood : RedBlood {
	Translation "16:47=112:127","185:191=11:12"
	Decal GreenBloodSplat
	States {
		Spawn:
			TNT1 AA 0 A_SpawnProjectile ("GreenFlyingBlood", 0, 0, random (0, 360), 2, random (0, 160))
			TNT1 AA 0 A_SpawnProjectile ("GreenFlyingBloodFast", 0, 0, random (0, 360), 2, random (0, 180))
			BLUD ABC 2
			Stop
	}
}

ACTOR RedFlyingBloodFast : RedFlyingBlood {
	Speed 6
	-DOOMBOUNCE
	+FORCEXYBILLBOARD
	-SOLID
	Scale 0.5
	Gravity 0.8
}

ACTOR BlueFlyingBlood : RedFlyingBlood
{
	Decal bluebloodsplat
	Translation "1:191=200:207","208:255=200:207"
}

ACTOR BlueFlyingBloodFast : RedFlyingBloodFast {
	Translation "1:191=200:207","208:255=200:207"
}

ACTOR GreenFlyingBlood : RedFlyingBlood {
	Decal greenbloodsplat
	Translation "168:184=112:127","185:191=11:12","16:47=112:127"
}

ACTOR GreenFlyingBloodFast : RedFlyingBloodFast {
	Decal GreenBloodSplat
	Translation "168:184=112:127","185:191=11:12","16:47=112:127"
}

ACTOR RedGibs {
	Radius 3
  Height 3
  Speed 7
  Scale 0.5
	Mass 1
  +NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	-SOLID
	+DONTOVERLAP
	+EXPLODEONWATER
	+DONTOVERLAP
	Decal Bloodsmear
	Gravity 0.7
  States {
    Spawn:
      GIBS C 3
      Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3")
		Death1:
			GIBS B 1
			TNT1 A 0 A_QueueCorpse
			GIBS B -1
			Stop
		Death2:
			BLD1 D 1
			TNT1 A 0 A_QueueCorpse
			BLD1 D -1
			Stop
		Death3:
			BLD1 C 1
			TNT1 A 0 A_QueueCorpse
			BLD1 C -1
			Stop
    }
}

Actor BigRedGibs : RedGibs {
	Scale 1.0
	+DONTOVERLAP
	+EXPLODEONWATER
}

ACTOR SmallRedGibs {
  Radius 3
  Height 3
  Speed 7
  Scale 0.2
	Mass 1
  +NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	-SOLID
	+DONTOVERLAP
	+EXPLODEONWATER
	+DONTOVERLAP
	Decal Bloodsmear
	Gravity 0.7
  States {
    Spawn:
      GIBS C 3
      Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3")
		Death1:
			GIBS B 1
			TNT1 A 0 A_QueueCorpse
			GIBS B -1
			Stop
		Death2:
			BLD1 D 1
			TNT1 A 0 A_QueueCorpse
			BLD1 D -1
			Stop
		Death3:
			BLD1 C 1
			TNT1 A 0 A_QueueCorpse
			BLD1 C -1
			Stop
    }
}

ACTOR BlueGibs {
  Radius 3
  Height 3
  Speed 7
  Scale 0.5
	Mass 1
	Translation "1:191=200:207","208:255=200:207"
  +NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	-SOLID
	+DONTOVERLAP
	+EXPLODEONWATER
	+DONTOVERLAP
	Decal Bloodsmear
	Gravity 0.7
  States {
    Spawn:
      GIBS C 3
      Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3")
		Death1:
			GIBS B -1
			Stop
		Death2:
			BLD1 D -1
			Stop
		Death3:
			BLD1 C -1
			Stop
    }
}

Actor BigBlueGibs : BlueGibs {
	Scale 1.0
	+DONTOVERLAP
	+EXPLODEONWATER
}

ACTOR SmallBlueGibs {
  Radius 3
  Height 3
  Speed 7
  Scale 0.2
	Mass 1
	Translation "1:191=200:207","208:255=200:207"
  +NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	-SOLID
	+DONTOVERLAP
	+EXPLODEONWATER
	+DONTOVERLAP
	Decal Bloodsmear
	Gravity 0.7
  States {
		Spawn:
			GIBS C 3
			Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3")
		Death1:
			GIBS B -1
			Stop
		Death2:
			BLD1 D -1
			Stop
		Death3:
			BLD1 C -1
			Stop
    }
}

ACTOR GreenGibs {
  Radius 3
  Height 3
  Speed 7
  Scale 0.5
	Mass 1
	Translation "168:184=112:127","185:191=11:12","16:47=112:127"
  +NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	-SOLID
	+DONTOVERLAP
	+EXPLODEONWATER
	+DONTOVERLAP
	Decal Bloodsmear
	Gravity 0.7
  States {
    Spawn:
      GIBS C 3
      Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3")
		Death1:
			GIBS B -1
			Stop
		Death2:
			BLD1 D -1
			Stop
		Death3:
			BLD1 C -1
			Stop
    }
}

Actor BigGreenGibs : GreenGibs {
	Scale 1.0
	+DONTOVERLAP
	+EXPLODEONWATER
}

ACTOR SmallGreenGibs {
  Radius 3
  Height 3
  Speed 7
  Scale 0.5
	Mass 1
	Translation "168:184=112:127","185:191=11:12","16:47=112:127"
  +NOBLOCKMAP
	+MISSILE
  +NOTELEPORT
  +MOVEWITHSECTOR
  +CLIENTSIDEONLY
  +BLOODLESSIMPACT
	+DONTSPLASH
	+FLOORCLIP
	-SOLID
	+DONTOVERLAP
	+EXPLODEONWATER
	+DONTOVERLAP
	Decal Bloodsmear
	Gravity 0.7
  States {
    Spawn:
      GIBS C 3
      Loop
		Death:
			TNT1 A 0 A_QueueCorpse
			TNT1 A 0 A_Jump(255, "Death1", "Death2", "Death3")
    Death1:
			GIBS B -1
			Stop
		Death2:
			BLD1 D -1
			Stop
		Death3:
			BLD1 C -1
			Stop
    }
}

Actor RedHitBlood Replaces Blood {
	Decal BrutalBloodSplat
	Scale 0.5
	Speed 0
	Game Doom
	+FORCEXYBILLBOARD
	+NOGRAVITY
	+THRUACTORS
	+CLIENTSIDEONLY
	-ALLOWPARTICLES
	States {
		Spawn:
			TNT1 AAA 0
			TNT1 A 0 A_Jump(230, "BloodRnd")
			TNT1 A 0 A_SpawnProjectile ("RedFlyingBlood", 0, 0, random (0, 360), 2, random (0, 160))
		BloodRnd:
			TNT1 A 0 A_SpawnProjectile ("RedBloodHitFlesh", 0, 0, random (0, 360), 2, random (0, 90))
			Stop
	}
}

Actor RedBloodHitFlesh {
	Height 5
	Radius 5
	+NOBLOCKMAP
	+NOGRAVITY
	+NOCLIP
	+CLIENTSIDEONLY
	Scale 0.5
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_Jump(256, "Blood1", "Blood2")
		Blood1:
			BIMP ABCDE 1
			Stop
		Blood2:
			BLHT ABCDE 1
			Stop
	}
}

Actor BlueHitBlood : RedHitBlood {
	Decal BrutalBloodSplat
	Scale 0.5
	Speed 0
	Game Doom
	+FORCEXYBILLBOARD
	+NOGRAVITY
	+THRUACTORS
	+CLIENTSIDEONLY
	-ALLOWPARTICLES
	States {
		Spawn:
			TNT1 AAA 0
			TNT1 A 0 A_Jump(230, "BloodRnd")
			TNT1 A 0 A_SpawnProjectile ("BlueFlyingBlood", 0, 0, random (0, 360), 2, random (0, 160))
		BloodRnd:
			TNT1 A 0 A_SpawnProjectile ("BlueBloodHitFlesh", 0, 0, random (0, 360), 2, random (0, 90))
			Stop
	}
}

Actor BlueBloodHitFlesh {
	Height 5
	Radius 5
	+NOBLOCKMAP
	+NOGRAVITY
	+NOCLIP
	+CLIENTSIDEONLY
	Scale 0.5
	Translation "1:191=200:207","208:255=200:207"
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_Jump(256, "Blood1", "Blood2")
		Blood1:
			BIMP ABCDE 1
			Stop
		Blood2:
			BLHT ABCDE 1
			Stop
	}
}

Actor GreenHitBlood : RedHitBlood {
	Decal BrutalBloodSplat
	Scale 0.5
	Speed 0
	Game Doom
	+FORCEXYBILLBOARD
	+NOGRAVITY
	+THRUACTORS
	+CLIENTSIDEONLY
	-ALLOWPARTICLES
	States {
		Spawn:
			TNT1 AAA 0
			TNT1 A 0 A_Jump(230, "BloodRnd")
			TNT1 A 0 A_SpawnProjectile ("GreenFlyingBlood", 0, 0, random (0, 360), 2, random (0, 160))
		BloodRnd:
			TNT1 A 0 A_SpawnProjectile ("GreenBloodHitFlesh", 0, 0, random (0, 360), 2, random (0, 90))
			Stop
	}
}

Actor GreenBloodHitFlesh {
	Height 5
	Radius 5
	+NOBLOCKMAP
	+NOGRAVITY
	+NOCLIP
	+CLIENTSIDEONLY
	Scale 0.5
	Translation "168:184=112:127","185:191=11:12","16:47=112:127"
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_Jump(256, "Blood1", "Blood2")
		Blood1:
			BIMP ABCDE 1
			Stop
		Blood2:
			BLHT ABCDE 1
			Stop
	}
}
