Actor CacoRemix : Cacodemon Replaces Cacodemon {
	Health 400
	GibHealth 30
	Radius 31
	Height 56
	Mass 400
	Speed 8
	BloodColor "Blue"
	BloodType "BlueHitBlood"
	Decal "BlueFlyingBloodSplat"
	PainChance 64
	PainChance "SuperBuckshot", 90
	PainChance "Saw", 128
	Monster
	+FLOAT
	+NOGRAVITY
	-FRIGHTENED
	SeeSound "caco/sight"
	PainSound "caco/pain"
	DeathSound "caco/death"
	ActiveSound "caco/active"
	Obituary "$OB_CACO"
	HitObituary "$OB_CACOHIT"
	States {
		Spawn:
			HEAD A 10 A_Look
			Loop
		See:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, "SeeVanilla")
			TNT1 A 0 A_ChangeFlag("COUNTKILL", TRUE)
			TNT1 A 0 A_ChangeFlag("SLIDESONWALLS", FALSE)
			HEAD A 3 A_Chase
			Loop
		SeeVanilla:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==0, "See")
			HEAD A 3 A_Chase(0, "MissileVanilla")
			Loop
		Melee:
			TNT1 A 0 A_ChangeFlag("SLIDESONWALLS", TRUE)
			HEAD A 0 A_FaceTarget
			HEAD A 1 A_Recoil (-8)
			HEAD ABCCCB 2 A_FaceTarget
			HEAD A 2 A_SpawnProjectile("CacoAttack",10,0,0,0)
			HEAD A 6
			HEAD A 0 A_Stop
			Goto See
		Missile:
			TNT1 A 0 A_JumpIfCloser(125, "Melee")
			HEAD BC 5 A_FaceTarget
			TNT1 A 0 A_JumpIf(CallACS("GetFastMonstProjectiles", 0, 0, 0)==1, "FastCacoBall")
			HEAD D 5 Bright A_CustomComboAttack("CacodemonBallRemix", 32, 10 * random(1, 6))
			Goto See
			FastCacoBall:
				HEAD D 5 Bright A_CustomComboAttack("FastCacodemonBallRemix", 32, 10 * random(1, 6))
				Goto See
		MissileVanilla:
			HEAD BC 5 A_FaceTarget
			HEAD D 5 Bright A_HeadAttack
			Goto See
		Dodge:
			HEAD A 0 A_Jump(128, 6)
			HEAD A 0 A_FaceTarget
			HEAD A 0 ThrustThing(angle*256/360+192, 6, 0, 0)
			HEAD E 3
			HEAD E 3 A_Pain
			HEAD F 6 A_Stop
			Goto See
			HEAD A 0 A_FaceTarget
			HEAD A 0 ThrustThing(angle*256/360+64, 6, 0, 0)
			HEAD E 3
			HEAD E 3 A_Pain
			HEAD F 6 A_Stop
			Goto See
		Pain:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, 2)
			HEAD A 0 A_Jump(192, "Dodge")
			HEAD E 3
			HEAD E 3 A_Pain
			HEAD F 6 A_Stop
			Goto See
		Death:
			TNT1 A 0 A_Jump(8, "Death.Headshot")
			TNT1 AA 0 A_SpawnItem("BlueBlood", 0, 30)
			TNT1 AA 0 A_SpawnProjectile("BlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AA 0 A_SpawnProjectile("SmallBlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			HEAD G 4
			HEAD H 4 A_Scream
			HEAD IJ 4
			HEAD K 4 A_NoBlocking
			HEAD L -1 A_SetFloorClip
			Stop
		Death.Plasma:
			TNT1 A 0 A_GiveInventory("PlasmaDeath", 1)
			CATB A 4 A_Scream
			CATB B 4 A_NoBlocking
			CATB CDE 4
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnItemEx("CellSmoke")
			CATB F -1 A_SetFloorClip
			Stop
		Death.Nato:
		Death.SuperBuckshot:
		Death.Buckshot:
			TNT1 A 0 A_Jump(8, "Death.Headshot")
			TNT1 A 0 A_GiveInventory("BuckDeath", 1)
			TNT1 AA 0 A_SpawnItem("BlueBlood", 0, 30)
			TNT1 AAA 0 A_SpawnProjectile("BlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AA 0 A_SpawnProjectile("SmallBlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			CATS A 4 A_Scream
			CATS B 4 A_NoBlocking
			CATS CDE 4
			CATS F 4
			CATS G -1 A_SetFloorClip
			Stop
		Death.Headshot:
			TNT1 A 0 A_GiveInventory("HSDeath", 1)
			TNT1 AA 0 A_SpawnItem("BlueBlood", 0, 30)
			TNT1 AA 0 A_SpawnProjectile("BlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AA 0 A_SpawnProjectile("SmallBlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			CAHS A 4 A_Scream
			CAHS B 4 A_NoBlocking
			CAHS CD 4
			CAHS E 4
			CAHS F -1 A_SetFloorClip
			Stop
		Death.SoulRes:
			TNT1 A 0 A_NoBlocking
			TNT1 A 0 A_JumpIfInventory("PlasmaDeath", 1, 4)
			TNT1 A 0 A_JumpIfInventory("ExDeath", 1, 4)
			TNT1 A 0 A_JumpIfInventory("BuckDeath", 1, 4)
			HEAD L -1
			Stop
			CATB F -1
			Stop
			CATE J -1
			Stop
			CATS G -1
			Stop
		XDeath:
			TNT1 A 0 A_GiveInventory("ExDeath", 1)
			TNT1 AA 0 A_SpawnItem("BlueBlood", 0, 30)
			TNT1 A 0 A_SpawnProjectile ("BigBlueGibs", 32, 0, random (0, 360), 2, random (0, 160))
			TNT1 AAA 0 A_SpawnProjectile("BlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AAA 0 A_SpawnProjectile("SmallBlueGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			CATE A 4 A_Scream
			CATE B 4 A_NoBlocking
			CATE CDEFGH 4
			CATE I 4
			CATE J -1 A_SetFloorClip
			Stop
		Raise:
			TNT1 A 0 A_CheckProximity("RaiseContinue", "ArchvileRemix", 100)
			TNT1 A 0 A_ChangeFlag("COUNTKILL", false)
			Goto SoulRaise
			RaiseContinue:
				TNT1 A 0 A_JumpIfInventory("PlasmaDeath", 1, "PlasRaise")
				TNT1 A 0 A_JumpIfInventory("BuckDeath", 1, "BuckRaise")
				TNT1 A 0 A_JumpIfInventory("ExDeath", 1, "ExRaise")
				TNT1 A 0 A_JumpIfInventory("HSDeath", 1, "HSRaise")
				HEAD L 1
				HEAD KJIHG 4
				Goto See
		PlasRaise:
			CATB F 1
			CATB EDCBA 4 A_TakeInventory("PlasmaDeath", 1)
			Goto See
		BuckRaise:
			CATS G 1
			CATS FEDCBA 4 A_TakeInventory("BuckDeath", 1)
			Goto See
		ExRaise:
			CATE J 1
			CATE IHGFEDCBA 4 A_TakeInventory("ExDeath", 1)
			Goto See
		HSRaise:
			CAHS F 1
			CAHS EDBCA 4 A_TakeInventory("HSDeath", 1)
			Goto See
		SoulRaise:
			HEAD L 0 A_SpawnItem("CacoSoul", 0, 0)
			HEAD L 0 A_Die("SoulRes")
			Stop
	}
}

Actor CacodemonBallRemix {
	Radius 6
	Height 8
	Speed 10
	FastSpeed 20
	Damage 5
	Projectile
	+RANDOMIZE
	RenderStyle Add
	Alpha 1
	SeeSound "caco/attack"
	DeathSound "caco/shotx"
	States
	{
	Spawn:
		BAL2 AB 4 Bright
		Loop
	Death:
		BAL2 CDE 6 Bright
		Stop
	}
}

Actor CacoAttack : ImpAttack2
{
    +THRUGHOST
    Damage 5
}

Actor FastCacodemonBallRemix : CacodemonBallRemix {
	Speed 20
}

Actor CacoSoul : CacoRemix {
	+SHADOW
	Health 40
	GibHealth 30
	Speed 5
	RenderStyle Translucent
	Alpha 0.5
	-SOLID
	+SHOOTABLE
	-COUNTKILL
	+NOBLOOD
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_JumpIfInventory("ExDeath", 1, "ExSpawn", AAPTR_MASTER)
			TNT1 A 0 A_JumpIfInventory("PlasmaDeath", 1, "PlasSpawn", AAPTR_MASTER)
			TNT1 A 0 A_JumpIfInventory("BuckDeath", 1, "BuckSpawn", AAPTR_MASTER)
			TNT1 A 0 A_JumpIfInventory("HSDeath", 1, "HSSpawn", AAPTR_MASTER)
			HEAD L 4 A_UnSetFloorClip
			HEAD KJIHG 4
			Goto Super::Spawn
		PlasSpawn:
			CATB F 4 A_UnSetFloorClip
			CATB EDCBA 4
			Goto Super::Spawn
		BuckSpawn:
			CATS G 4 A_UnSetFloorClip
			CATS FEDCBA 4
			Goto Super::Spawn
		ExSpawn:
			CATE J 4 A_UnSetFloorClip
			CATE IHGFEDCBA 4
			Goto Super::Spawn
		HSSpawn:
			CAHS F 4 A_UnSetFloorClip
			CAHS EDCBA 4
			Goto Super::Spawn
		Death:
			HEAD G 4 A_FadeOut(0.20)
			HEAD H 4 A_Scream
			HEAD IJ 4 A_FadeOut(0.15)
			HEAD K 4 A_FadeOut(0.15)
			FadeD:
				HEAD L 1 A_FadeOut(0.15)
				Loop
		Death.Plasma:
			CATB A 4 A_Scream
			CATB B 4 A_FadeOut(0.15)
			CATB CDE 4 A_FadeOut(0.20)
			FadePD:
				CATB F 1 A_FadeOut(0.15)
				Loop
		Death.Nato:
		Death.SuperBuckshot:
		Death.Buckshot:
			CATS A 4 A_Scream
			CATS B 4 A_FadeOut(0.15)
			CATS CDEF 4 A_FadeOut(0.15)
			FadeBS:
				CATS G 1 A_FadeOut(0.15)
				Loop
		XDeath:
			CATE A 4 A_Scream
			CATE B 4 A_FadeOut(0.15)
			CATE CDEFGHI 4 A_FadeOut(0.15)
			FadeXD:
				CATE J 1 A_FadeOut(0.15)
				Loop
		Raise:
			Stop
	}
}
